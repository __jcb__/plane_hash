from .point import Point

class HyperPlane:
    def __init__(self, coordinate_factors: iter, result: int):
        self.coordinate_factors = coordinate_factors
        self.result = result

    # check if a piont is in a plane
    def check_for_point(self, point_coordinates):
        return self.get_result(point_coordinates) == self.result

    # all coordinates are inserted an the result is returned
    def get_result(self, coordinates, skip_last=False):
        # to ignore the last one
        if skip_last:
            coordinates_factors = self.coordinate_factors
        else:
            coordinates_factors = self.coordinate_factors[0:-1]

        # add up
        plane_term_sum = 0
        for own, other in zip(self.coordinate_factors, coordinates):
            plane_term_sum += own * other
        return plane_term_sum

    def fill_up_point(self, coordinates):
        first_coordinates_sum = self.get_result(coordinates, skip_last=True)    # add up the first
        last_coordinate = (self.result - first_coordinates_sum) / self.coordinate_factors[-1]   # calc the last one
        return Point(list(coordinates) + [last_coordinate])

    @property
    def dimantions(self):
        return len(self.coordinate_factors)

    def __repr__(self):
        op = ""
        for c, i in enumerate(self.coordinate_factors):#
            sign = ""
            if i >= 0:
                sign = "+"
            op += f"{sign}{i}x{c} "
        return f"P: {op[1:]} = {self.result}"




