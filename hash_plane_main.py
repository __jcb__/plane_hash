from geometry.plane import HyperPlane
from geometry.point import Point
from secrets import randbelow

PLANE_DIMANSIONS = 4
MIN_BIT_PER_DIMANSION = 8
RANDOM_NUM_BELOW = 100

class PlaneHashVerifier:
    def get_bit_stream(self, text):
        text_byte_array = text.encode()
        # print(tuple(map(lambda x: bin(x), text_byte_array)))
        op = 0

        # concat all numbers
        for c, i in enumerate(text_byte_array):
            op += i * 256 ** (len(text) - c -1)

        bin_str = bin(op)[2:]
        cleaned_bin_str = bin_str

        # fill up until all coordinates are sized equal
        if (overflow := (len(bin_str) % (MIN_BIT_PER_DIMANSION * PLANE_DIMANSIONS))) != 0:
            gap_counter = MIN_BIT_PER_DIMANSION * PLANE_DIMANSIONS - overflow

            repeation_factor = 2
            # repeat all bits twice with gape fill in
            while gap_counter > 0:
                curernt_bit_posion = 0
                while gap_counter > 0 and curernt_bit_posion < len(bin_str):
                    cleaned_bin_str += bin_str[curernt_bit_posion] * repeation_factor + bin(repeation_factor + curernt_bit_posion+64 + int(bin_str[curernt_bit_posion]))[-5:]
                    curernt_bit_posion += 1
                    gap_counter -= 4 + repeation_factor
                repeation_factor += 1
            cleaned_bin_str = cleaned_bin_str[:-(len(cleaned_bin_str) % (MIN_BIT_PER_DIMANSION * PLANE_DIMANSIONS))]
        # print(len(cleaned_bin_str), cleaned_bin_str, "\n", len(bin_str),  bin_str)
        return cleaned_bin_str


    def split_in_coordinates(self, bit_stream):
        # could be a different encoding to coordinates (maybe with randomness)
        # coord1 = first part; coord2 = secound part; ...
        op = []
        dim_bit_len = int(len(bit_stream) / PLANE_DIMANSIONS)
        for i in range(PLANE_DIMANSIONS):
            op.append(bit_stream[i * dim_bit_len : (i+1) * dim_bit_len])
        return tuple(map(lambda x: int(x, 2), op))

    def set_up_plane(self, plane_coordinates, result):
        return HyperPlane(plane_coordinates, result)

    def get_random_point_in_plane(self, plane: HyperPlane):
        random_input = []
        # generate all but one coord
        for i in range(plane.dimantions - 1):
            random_input.append(randbelow(RANDOM_NUM_BELOW))
        # calc last one and return
        return plane.fill_up_point(random_input)


    def verify(self, string: str, point: Point):
        plane = generate_plane(string)
        return plane.check_for_point(point)


    def generate_plane(self, string: str):
        bit_stream = get_bit_stream(string)
        coordinate_factors = split_in_coordinates(bit_stream)
        result = int(bit_stream, 2) / (PLANE_DIMANSIONS * MIN_BIT_PER_DIMANSION)
        return set_up_plane(coordinate_factors, result)

    def generate_random_point(self, string: str):
        plane = generate_plane(string)
        return get_random_point_in_plane(plane)
